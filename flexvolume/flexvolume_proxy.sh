#!/bin/bash

# This script provides a proxy flexvolume driver that forwards commands to a container.
# This enables support for Cloud-Infrastructure's docker-volume-cvmfs and -eos.
# The init call cannot be forwarded as volume containers implemented as Daemonsets are not
# running yet when origin-node initializes drivers.

# see https://docs.openshift.org/latest/install_config/persistent_storage/persistent_storage_flex_volume.html#flex-volume-drivers-without-master-initiated-attach-detach
# and https://github.com/kubernetes/kubernetes/blob/894b9b2add77034772cc8860857ca566a1eee110/examples/volumes/flexvolume/nfs#L86

# name of the docker container where the flexvolume driver is running
VOLUME_DOCKER_CONTAINER_NAME=docker-volume-cvmfs
# path to the flexvolume driver binary/script inside that container
VOLUME_DRIVER_PATH=/usr/sbin/docker-volume-cvmfs

usage() {
    err "Invalid usage. Usage: "
    err "\t$0 init"
    err "\t$0 mount <mount dir> <mount device> <json params>"
    err "\t$0 unmount <mount dir>"
    exit 1
}

err() {
    echo -ne $* 1>&2
}

log() {
    echo -ne $* >&1
}

init() {
    # Daemonset not running yet. Cannot forward this.
    log "{\"status\": \"Success\", \"capabilities\": {\"attach\": false, \"selinuxRelabel\": false}}"
    exit 0
}


forward-call() {
    hash docker jq 2>/dev/null || { err "{\"status\": \"Failure\", \"message\": \"Docker or jq command not available\"}"; exit 1; }
    test -z $(docker ps -qf "name=^/${VOLUME_DOCKER_CONTAINER_NAME}$") && { err "{\"status\": \"Failure\", \"message\": \"${VOLUME_DOCKER_CONTAINER_NAME} container not running\"}"; exit 1; }
    docker exec --privileged "${VOLUME_DOCKER_CONTAINER_NAME}" "${VOLUME_DRIVER_PATH}" "$@"
}

op=$1

case "$op" in
    "")
        usage
        ;;
    init)
        init
        ;;
    mount|unmount)
        forward-call "$@"
        ;;
    *)
        log "{ \"status\": \"Not supported\" }"
 		exit 1
esac
