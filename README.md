# cvmfs-daemonset

## Overview

This is a daemonset enabling CVMFS support for applications in Openshift. The daemonset starts, manages and monitors a container running Cloud team's [docker-volume-cvmfs](https://gitlab.cern.ch/cloud-infrastructure/docker-volume-cvmfs).
The *docker-volume-cvmfs* in turns provides a Docker volume plugin and Kubernetes flexvolume driver providing CVMFS volumes.

## How it works

The deployment of *docker-volume-cvmfs* is different than what the [Cloud team is doing for Magnum](https://gitlab.cern.ch/cloud-infrastructure/magnum/blob/cern-newton/magnum/drivers/common/templates/fragments/configure-cvmfs.sh).

In Magnum [docker-volume-cvmfs](https://gitlab.cern.ch/cloud-infrastructure/docker-volume-cvmfs) is deployed outside Kubernetes, as
an atomic container. While we could deploy it in a similar way on Puppet-managed nodes via `puppet::run`, it is more interesting to
deploy it as a DaemonSet:
* we can specify ressource requests, which are removed from the host's allocatable resources. It is more flexible to have the resource requested by the DaemonSet's pods (even if these resources are actually consumed by the *docker-volume-cvmfs* container rather than the daemonset;s pod) than statically reserving some memory in the origin-node/Kubelet parameters.
* a DaemonSet is convenient to modify and redeploy; great flexibility in deciding where it runs or not with node labels.
* we can easily monitor *docker-volume-cvmfs*
* no dependency on Puppet or other deployment mechanisms

But there are some drawbacks:
* a flexvolume driver must be present in `/usr/libexec/kubernetes/kubelet-plugins/volume/exec` *before* the origin-node/Kubelet starts. This means it cannot be deployed by the daemonset and instead the flexvolume must be present beforehand. So it is still necessary to deploy the flexvolume driver with other means.
* for the same reason, instead of directly using the *docker-volume-cvmfs* binary as a flexvolume driver we use a
[proxy script](flexvolume/flexvolme_proxy.sh) that forwards the flexvolume calls to the *docker-volume-cvmfs* container (provided it's running)
* the daemonset pod is subject to possible eviction in case of [resource exhaution](https://docs.openshift.org/latest/admin_guide/out_of_resource_handling.html#out-of-resource-best-practice-daemonset),
which is especially problematic as CVMFS fuse mount points will be invalidated (`Transport endpoint not connected` error)
and pods using a bind-mount of the fuse mount points are not recoverable: the pod must be completely deleted and re-created for the
CVMFS volume to work again. To avoid this, make sure to use the `Guaranteed` QoS tier.

## Deployment

The `cvmfs-daemonset` is deployed in the `paas-infra` namespace. For instructions on how
to deploy the namespace, check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29)

The pod needs to access the host network and mount host paths
```
oc create serviceaccount cvmfs -n paas-infra
oc adm policy add-scc-to-user privileged system:serviceaccount:paas-infra:cvmfs
```

Deploy the DaemonSet:
```
oc create -f daemonset.yaml -n paas-infra
```

The CVMFS daemonset will be deployed to nodes with label `cvmfs=true`. This label is typically set at node creation for nodes in the `standard` role.

## Continuous integration/deployment

The deployment of the DaemonSet is managed by GitLab-CI. Any changes to `daemonset.yaml` will
be automatically redeployed into dev and production when merged to `master`. While changes are applied
to the daemonset, this will NOT restart the existing pods as this would interrupt the
CVGMFS fuse processes and renter existing CVMFS mounts invalid. Changes are thus only applied after the pods
are re-created (e.g. following node evacuation)

The deployment the `paas-infra` project should provide a serviceaccount `paas-infra-sync` with
appropriate permissions for this purpose.

Store the token (`oc serviceaccounts get-token paas-infra-sync -n paas-infra`) for the service account in GitLab
Secure Variables `PAAS_INFRA_SYNC_TOKEN_DEV` and `PAAS_INFRA_SYNC_TOKEN_PROD` for dev and prod clusters respectively.

### Integration tests on Openshift CI runner

Testing the daemonset requires a GitLab-CI runner running on a host with Docker and certain configuration (see comments in [test/openshift/prepare_cluster.sh](test/openshift/prepare_cluster.sh) for details).
An appropriate runner is provided in the CI nodes from the [paas/ci subhostgroup](https://espace.cern.ch/openshift-internal/_layouts/OneNote.aspx?id=%2Fopenshift-internal%2FShared%20Documents%2FOpenshift&wd=target%28Deployment.one%7CDACB26C0-65A0-4EEE-8B63-99CFEAEF4A66%2FCI%3A%20dedicated%20runner%20for%20CI%20of%20Openshift%20infrastructure%20component%7C19A61F87-1164-4748-B679-3FFD6CED1306%2F%29), so it just needs to be enabled for this project.

To run the job we also need an image with the latest oc CLI and the docker client. We use the [`openshift-client`](https://gitlab.cern.ch/paas-tools/openshift-client) image.

### Manual runner configuration
The [specific runner](https://docs.gitlab.com/runner/install/docker.html) must be configured with `privileged`, `network=host` and with access to the host's Docker socket. This runner is expects to use the `openshift-cluster-ci` tag.

The host also needs (assuming CC7):
* `MountFlags=shared` in `/usr/lib/systemd/system/docker.service` (CC7 default: slave)
* `INSECURE_REGISTRY='--insecure-registry 172.30.0.0/16'` in `/etc/sysconfig/docker`
* `semanage boolean virt_use_fusefs --modify --on && semanage boolean virt_sandbox_use_fusefs --modify --on`
Also for ad-hoc (non Puppet-managed) machines the following might be necessary as per https://github.com/openshift/origin/blob/master/docs/cluster_up_down.md#linux
```bash
firewall-cmd --permanent --new-zone dockerc && \
    firewall-cmd --permanent --zone dockerc --add-source 172.17.0.0/16 && \
    firewall-cmd --permanent --zone dockerc --add-port 8443/tcp && \
    firewall-cmd --permanent --zone dockerc --add-port 53/udp && \
    firewall-cmd --permanent --zone dockerc --add-port 8053/udp && \
    firewall-cmd --reload
```



