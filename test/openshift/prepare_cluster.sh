#!/usr/bin/env bash
# tests the docker volume in Openshift
# Requires a runner with privileged, network=host and bind-mount of the host's Docker socket and an
# image with the latest oc CLI and the docker client.
# The host also needs:
# MountFlags=shared in /usr/lib/systemd/system/docker.service [CC7 default: slave]
# INSECURE_REGISTRY='--insecure-registry 172.30.0.0/16' in /etc/sysconfig/docker
# semanage boolean virt_use_fusefs --modify --on && semanage boolean virt_sandbox_use_fusefs --modify --on
# Also for ad-hoc machines the following might be necessary as per https://github.com/openshift/origin/blob/master/docs/cluster_up_down.md#linux
# firewall-cmd --permanent --new-zone dockerc && firewall-cmd --permanent --zone dockerc --add-source 172.17.0.0/16 && firewall-cmd --permanent --zone dockerc --add-port 8443/tcp && firewall-cmd --permanent --zone dockerc --add-port 53/udp && firewall-cmd --permanent --zone dockerc --add-port 8053/udp && firewall-cmd --reload

set -e

# use defaults if not specified (use version from oc client if not specified)
OPENSHIFT_IMAGE=${OPENSHIFT_IMAGE:-openshift/origin}
OPENSHIFT_VERSION=${OPENSHIFT_VERSION:-$(oc version | grep -oP '(?<=oc )[\w\.]+')}

custom_openshift_image_name='openshift-with-cvmfs-flexvolume'
scriptdir=$(dirname "${BASH_SOURCE[0]}")

sed "s|^FROM .*|FROM ${OPENSHIFT_IMAGE}:${OPENSHIFT_VERSION}|" -i "${scriptdir}/origin-with-cvmfs-dockerfile"
docker build -t "${custom_openshift_image_name}:${OPENSHIFT_VERSION}" --file "${scriptdir}/origin-with-cvmfs-dockerfile" .

# we also need -pod and -deployer images with the same name, but not need to modify them
for image_suffix in pod deployer; do
  docker pull ${OPENSHIFT_IMAGE}-${image_suffix}:${OPENSHIFT_VERSION}
  docker tag ${OPENSHIFT_IMAGE}-${image_suffix}:${OPENSHIFT_VERSION} "${custom_openshift_image_name}-${image_suffix}:${OPENSHIFT_VERSION}"
done

oc cluster down || echo "No cluster previously running."
oc cluster up --image=${custom_openshift_image_name} --version=${OPENSHIFT_VERSION}

oc login -u system:admin

# create daemonset starting the docker-volume-cvmfs container
oc create serviceaccount cvmfs --namespace default
oc adm policy add-scc-to-user privileged system:serviceaccount:default:cvmfs
oc create --namespace default -f daemonset.yaml
# deploy the daemonset on all nodes
oc label nodes --all cvmfs=true

# create nginx app in standard namespace 'myproject'.
# To simplify, use anyuid SCC
oc adm policy add-scc-to-user anyuid system:serviceaccount:myproject:default
# create a pre-bound Flexvolume PV
oc create -f - <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-cvmfs-sft
spec:
  accessModes:
  - ReadOnlyMany
  capacity:
    storage: 1Mi
  claimRef:
    apiVersion: v1
    kind: PersistentVolumeClaim
    name: pvc-cvmfs
    namespace: myproject
  flexVolume:
    driver: cern/cvmfs
    options:
      repository: sft.cern.ch
  persistentVolumeReclaimPolicy: Retain
EOF
# Set up a matching PVC for CVMFS
oc create --namespace myproject -f - <<EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-cvmfs
spec:
  accessModes:
  - ReadOnlyMany
  resources:
      requests:
        storage: 1Mi
  # disable dynamic provisioning
  volumeName: pv-cvmfs-sft
EOF

oc new-app --namespace myproject nginx
oc volumes dc/nginx -n myproject --add -t pvc -m /usr/share/nginx/html --claim-name pvc-cvmfs
# set a liveness probe. Without this, pod cannot know its cvmfs volume is gone.
oc set probe dc/nginx -n myproject --readiness --get-url http://:80/lcg/mapfile.txt
# start 2 pods. This makes sure cvmfs works for multiple pods
oc scale dc/nginx -n myproject --replicas=2

# wait for deployment of the nginx app
sleep 5s
oc rollout status dc/nginx --namespace myproject

